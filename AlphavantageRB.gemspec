#lib = File.expand_path("../lib", __FILE__)
#$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
#require "rake"
$:.push File.expand_path('../lib', __FILE__)

Gem::Specification.new do |s|
  s.name        = "alphavantagerb"
  s.version	    = "1.3.1"
  s.authors     = ["Stefano Martin"]
  s.email       = ["stefano.martin87@gmail.com"]
  s.homepage    = "https://github.com/StefanoMartin/AlphaVantageRB"
  s.license     = "MIT"
  s.summary     = "A gem for Alpha Vantage"
  s.description = "A ruby wrapper for Alpha Vantage's HTTP API"
  s.platform	   = Gem::Platform::RUBY
  s.require_paths = ["lib"]
  #s.files         = FileList["lib/*", "spec/**/*", "AlphavantageRB.gemspec", "Gemfile", "LICENSE.md", "README.md"].to_a
  s.files         = Dir["lib/*"] + Dir["spec/**/*"] + Dir["AlphavantageRB.gemspec"] + Dir["Gemfile"] + Dir["LICENSE.md"] + Dir["README.md"]
  s.add_dependency "httparty", "0.16.2"
  s.add_dependency "humanize", "1.7.0"
end
